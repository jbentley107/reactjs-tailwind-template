/**
 * @file HeaderBar.js
 */
import * as React from 'react'
import PropTypes from 'prop-types'

// Import components
import {ReactComponent as NTTLogo} from '../../assets/SVG/ntt_logo.svg';

const HeaderBar = (props, { hasMenuButton }) => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  if (!hasMenuButton) {
    return (
      <Tag className={`header-bar header-bar--${variant} ${className}`}>
        <header className="bg-health-screening-header fixed inset-x-0 top-0">
          <div className="ntt-logo w-1/12 pl-8 py-4 align-middle inline-block">
            <NTTLogo />
          </div>
          <h2 className="text-white font-semibold text-xl leading-snug inline-block w-4/12 pl-8 align-middle">Health screening</h2>
        </header>
        {children}
        <style jsx>{`
          .header-bar {

          }

          .ntt-logo svg {
            width: 67px;
            height: 24px;
          }
        `}</style>
      </Tag>
    )
  }
}

HeaderBar.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
}

HeaderBar.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default HeaderBar;
