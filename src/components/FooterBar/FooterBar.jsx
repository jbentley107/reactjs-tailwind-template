/**
 * @file FooterBar.js
 */
// Import dependencies
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types'

// Import components
import {ReactComponent as Clock} from '../../assets/SVG/clock.svg';
import StatusIcon from '../StatusIcon/index';

const FooterBar = (props) => {
  const {
    currentStatus,
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  let day = moment().format('dddd');
  let date = moment().format('MMMM Do');
  let year = moment().format('YYYY');

  const [screeningMessage, setScreeningMessage] = useState('Screening in progress');

  let dateString = (day + ' ' + date + ', ' + year);

  useEffect(() => {
    if (currentStatus === 'proceed') {
      setScreeningMessage('Screening in progress');
    } else if (currentStatus === 'denied') {
      setScreeningMessage('Screening in progress');
    } else if (currentStatus === 'pending') {
      setScreeningMessage('Screening in progress');
    }
  }, [currentStatus])

  return (
    <Tag className={`footer-bar footer-bar--${variant} ${className}`}>
      <footer className="bg-health-screening-header fixed inset-x-0 bottom-0 flex">
        {/* Left Column */}
        <div className="left-side w-9/12 pl-4 py-4 flex-initial float-left text-left">
          <div className="inline-block">
            <StatusIcon status={currentStatus}/>
            <p className="text-white inline align-middle">{screeningMessage}</p>
          </div>
        </div>
        {/* Right Column */}
        <div className="right-side w-3/12 py-4 flex-initial align-middle text-right float-right md:pl-4 lg:pl-1 xl:">
          <div className="flex">
            <div className="clock md:w-1/6 lg:w-1/6 xl:w-12 pr-4 align-middle">
              <Clock />
            </div>
            <div>
              <p className="text-white">{ dateString }</p>
            </div>
          </div>
        </div>
      </footer>
      {children}
      <style jsx>{`
        .footer-bar {

        }

        .clock svg {
          fill: #959595;
          width: 24px;
          height: 24px;
        }
      `}</style>
    </Tag>
  )
}

FooterBar.propTypes = {
  currentStatus: PropTypes.string,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

FooterBar.defaultProps = {
  currentStatus: 'passed',
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default FooterBar
