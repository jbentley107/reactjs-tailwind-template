/**
 * @file StatusIcon.js
 */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Loader } from 'react-loaders';

// Import components
// import {ReactComponent as Passed} from '../../assets/SVG/checkmark.svg';

const StatusIcon = (props) => {
  const {
    status,
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  const [currentStatus, setCurrentStatus] = useState(status);

  const iconClasses = 'w-1/6 align-middle inline px-3';

  useEffect(() => {
    setCurrentStatus(status);
  }, [status])

  if (currentStatus === 'denied') {
    let variant = 'denied';
    return (
      <Tag className={`status-icon status-icon--${variant} ${className} failed ${iconClasses}`}>
        <Loader 
          type="ball-spin-fade-loader" 
          active 
          style={{transform: 'scale(0.3)'}}
        />
        {children}
        <style jsx>{`
          .status-icon--denied svg {
            fill: #959595;
            width: 24px;
            height: 16px;
          }
          .status-icon .loader-active {
            display: inline-block;
            margin-bottom: 5px;
            padding-right: 2rem;
          }
        `}</style>
      </Tag>
    )
  } else if (currentStatus === 'passed') {
    let variant = 'passed';
    return (
      <Tag className={`status-icon status-icon--${variant} ${className} checkmark ${iconClasses}`}>
        <Loader 
          type="ball-spin-fade-loader" 
          active 
          style={{transform: 'scale(0.3)'}}
        />
        {children}
        <style jsx>{`
          .status-icon--passed svg {
            fill: #959595;
            width: 24px;
            height: 16px;
          }
          .status-icon .loader-active {
            display: inline-block;
            margin-bottom: 5px;
            padding-right: 2rem;
          }
        `}</style>
      </Tag>
    )
  } else if (currentStatus === 'pending') {
    let variant = 'pending';
    return (
      <Tag className={`status-icon status-icon--${variant} ${className} checkmark ${iconClasses}`}>
        <Loader 
          type="ball-spin-fade-loader" 
          active 
          style={{transform: 'scale(0.3)'}}
        />
        {children}
        <style jsx>{`
          .status-icon .loader-active {
            display: inline-block;
            margin-bottom: 5px;
            padding-right: 2rem;
          }
        `}</style>
      </Tag>
    )
  }
}

StatusIcon.propTypes = {
  status: PropTypes.string,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

StatusIcon.defaultProps = {
  status: 'passed',
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default StatusIcon
